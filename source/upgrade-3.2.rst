Upgrading to Mailman 3
======================

This includes the requirements to upgrade to Mailman 3 suite, which includes:

- `Mailman Core <https://pypi.org/project/mailman/>`_ 3.x
- `Postorius <https://pypi.org/project/postorius/>`_ 1.x
- `MailmanClient <https://pypi.org/project/mailmanclient/>`_ 3.x
- `Django-mailman3 <https://pypi.org/project/django-mailman3/>`_ 1.x
- `Hyperkitty <https://pypi.org/project/Hyperkitty/>`_ 1.x

If you are interested in the full change log for each component, please refer to
documentation of each project.


Python 3
--------
Postorius, Django-mailman3 and Hyperkitty have been ported to Python 3 now. To
upgrade from a previous version, you would need to setup your Django project to
use Python 3.5+. Django 1.11+ are supported.

If you are using virtual environments, you can now run the whole stack from a
single virtual environment, unlike before, when you needed separate ones for
Django and Mailman Core.


Migration Steps
---------------

The migration for your Mailman installation depends on how you got everything
running.

- If you are using system packages, they should handle the upgrade process
  themselves. You *may* need to perform some changes after the upgrade is done,
  see the sections below for those steps.

- If you are using the packages directory from PyPI or source, you can do a
  ``pip update -U mailman`` to install Mailman Core. For Django packages, you
  would need to create a new virtual-environment (if you are using it), or
  install it using Python 3 pip.

- For Django apps, the usual steps for migrations include::

    $ python manage.py migrate
    $ python manage.py collectstatic
    $ python manage.py compress

- Mailman Core should handle database migration on its own when the new version
  is started.


Configuration
-------------

Postorius added a new *required* configuration flag
:setting:`POSTORIUS_TEMPLATE_BASE_URL` which should be set to URL that
Postorius is expected to be listening at. You should set it to whatever URL
your WSGI server is listening at.


Extra Migration Steps
---------------------

- Full text index for Hyperkitty needs to be re-built since our indexing engine
  doesn't maintain compatibility between index created in Python 2 and Python 3.

  Simplest way to do this to run ``python manage.py rebuild_index`` in your
  Django project. Note that if your project has a huge number of lists, this
  will take a lot of time.

  A new command ``python manage.py update_index_one_list <listname@example.com>``
  was added to Hyperkitty so that you can rebuild your index one-by-one, if you
  prefer that.
